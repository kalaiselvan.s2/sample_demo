<div class="modal fade" id="add_popup" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="add_product" class="form-horizontal" enctype="multipart/form-data" method="post">
            @csrf
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-4">Name:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control name" name="name" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-4">Short-Description:</label>
              <div class="col-sm-8">
                <textarea class="form-control short_desc" name="short_description" required></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-4">Description:</label>
              <div class="col-sm-8">
                <textarea class="form-control description" name="description" required></textarea> 
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-4">Category:</label>
              <div class="col-sm-8">
                <select class="form-control category" name="category_id" required>
                  <option value="1">Laptop</option>
                  <option value="2">Mobile</option>
                  <option value="3">Camera</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-4">Upload Images:</label>
              <div class="col-sm-8">
                <input type="file" class="form-control images_add" id="inputGroupFile02" multiple name="images[]">
              </div>
            </div>
            <div class="d-flex w-100 pb-2">
              <button class="btn btn-primary w-50 mx-auto">Create</button>
            </div>
            <span id="success_msg" style="padding: 5px; margin: 5px; color: green"></span>
          </form>
        </div>
      </div>
    </div>
</div>