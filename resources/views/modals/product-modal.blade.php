<div class="modal fade" id="view_popup" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">View Product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-4">Name:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control view_name" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-4">Short-Description:</label>
            <div class="col-sm-8">
              <textarea class="form-control view_short_desc" disabled></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-4">Description:</label>
            <div class="col-sm-8">
              <textarea class="form-control view_desc" disabled></textarea> 
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-4">Category:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control view_category" disabled>
            </div>
          </div>
          <div class="d-flex image-view justify-content-between">
            {{-- <div class="w-25">
              <img src="" alt="" class="img-fluid">
            </div> --}}
          </div>
        </div>
      </div>
    </div>
</div>