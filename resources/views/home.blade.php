@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('List') }}
                    <button class="btn btn-primary btn-add float-right"  data-toggle="modal" data-target="#add_popup">Add</button>                
                </div>
                <div class="card-body">
                    <table class="table table-bordered w-100 table-striped text-center">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Description</th>
                                <th>More..</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("modals.product-modal")
@includeIf("modals.add-modal")
@endsection
@section('scripts')
<script>
    var table = $(".table").DataTable({
        lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
      "ajax":"{{ route('product_list') }}",
      "columns": [
            {   "data": "id" ,
            },
            { 
                "data": "name",
            },
            { 
                "data": "category_name",
            },
            { "data": "description" },
            { 
                "data": "id",
                "render": function ( data, type, row, meta ) {
                    var template = `<button type="button" class="btn btn-primary view_toggle" data-toggle="modal" data-target="#view_popup"  data-id=${data} data-url="view/${data}"><i class="fas fa-eye text-light"></i></button>`
                    return template;
                }
            },
        ],
        stateSave: true,
    });
    table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();

    $(document).on("click",".view_toggle",function(){
        // var url = `modules/post-edit/`+$(this).attr("data-id");
        // $(".module-url").val(url);
        var getcurrentdataurl = $(this).attr("data-url");
        console.log("getcurrentdataurl",getcurrentdataurl)
        $.ajax({
            url: getcurrentdataurl,
            type:"GET",
            success: function(response){
                var data= response.data;
                console.log(data,"data");
                $('.view_name, .view_short_desc, .view_desc, .view_category').empty();
                $(".view_name").val(data.name);
                $(".view_short_desc").val(data.short_description);
                $(".view_desc").val(data.description);
                $(".view_category").val(data.category_name);
                var images = data.img_name.split("|");
                var temp =``;
                console.log(images)
                if(images.length > 0){
                    images.map((item,index)=>{
                        console.log(item,index);
                        temp += `<div class="w-25">
                            <img src="uploads/${item}" alt="" class="img-fluid" style="height:100px !important">
                        </div>`;
                        $(".image-view").html(temp);
                    })
                }
            }
        });
    })
    $("#add_product").on("submit",function(e){
        var form = $(this);
        var formdata =  new FormData(form[0]);
        var url = "{{route('product_add')}}";
        $.ajax({
            url: url,
            type:"POST",
            data:formdata,
            cache : false,
            processData:false,
            enctype: 'multipart/form-data',
            contentType: false,
            success: function(response){
                console.log(response);
                $('#success_msg').html('Successfully Added')
                table.ajax.reload();
                // $('#add_popup').close();
                $('.name, .short_desc, .description, .category, .images_add').val("");

            }
        });
        e.preventDefault();
    })
</script>
@endsection