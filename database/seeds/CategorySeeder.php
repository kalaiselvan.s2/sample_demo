<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array =[
            [
                'category_name' => 'Laptop'
            ],
            [
                'category_name' => 'Mobile'
            ],
            [
                'category_name' => 'Camera'
            ]
        ];
        foreach ($array as $item) {
            DB::table('product_categories')->insert([
                'category_name' => $item['category_name'],
                'created_at' => now()
            ]);
        }
    }
}
