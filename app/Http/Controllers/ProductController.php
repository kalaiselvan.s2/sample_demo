<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = Auth::user()->id;
        $products = Product::select('products.id as id', 'products.name as name', 'product_categories.category_name', 'products.description as description')->leftjoin('product_categories','product_categories.id','=','products.category_id')->where('products.user_id', $user_id)->orderBy('products.id', 'desc')->get();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$products
            ]);
        }
        return view('home',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        if($request->ajax()){
            $data=[
                'name'=>$request['name'],
                'short_description'=>$request['short_description'],
                'description'=>$request['description'],
                'category_id'=>$request['category_id'],
                'user_id'=> $user_id,
                'status'=> 1,
                'created_at' => now()
            ];
            $product = Product::create($data);
            // return $product->id;
            // IMG
            $images=array();
            if($files=$request->file('images')){
                foreach($files as $file){
                    $name=$file->getClientOriginalName();
                    $file->move('uploads',$name);
                    $images[]=$name;
                }
            }
            /*Insert your data*/
            ProductImage::insert( [
                'img_name'=>  implode("|",$images),
                'product_id' =>$product->id,
                'created_at' =>now()
                //you can put other insertion here
            ]);

            return response()->json([
                "code"=>200,
                // "data"=>$products
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $products = Product::leftjoin('product_images','product_images.product_id','=','products.id')->leftjoin('product_categories','product_categories.id','=','products.category_id')->where("products.id",$id)->first();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$products
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
