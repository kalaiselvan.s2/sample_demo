<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name', 'short_description', 'description', 'category_id', 'user_id', 'status', 'created_at'
    ];
}
